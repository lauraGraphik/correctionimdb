<?php

namespace App\DataFixtures;

use App\Entity\Film;
use App\Entity\User;
use App\Entity\Artiste;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername("admin")
            ->setEmail("admin@admin.fr")
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword( $this->encoder->encodePassword( $user, "mdp"));
        $manager->persist($user);
            
        // $artistes=[
        //     ["Brad", "Pitt"],
        //     ["George", "Clonney"],
        //     ["Matt", "Damon"],
        //     ["Julia", "Roberts"],
        //     ["Ben", "Affelck"],
        //     ["Albert", "Finney"],
        // ];
       

        // $films=[
        //     ["Will Hunting", "1998","Un pauvre se découvre génie des maths", "willHunting.jpg",$artiste ],
        //     ["Ocean's Eleven", "2001","Braquage d'un casino", "oceans11.jpg",$artiste ],
        //     ["Ocean's Twelve", "2004","Braquage d'un casino", "oceans12.jpg",$artiste ],
        //     ["Ocean's Thirteen", "2007","Braquage d'un casino", "oceans12.jpg",$artiste ],
        //     ["Erin Brokovitch", "2000","Braquage d'un casino", "oceans12.jpg",$artiste ],
        // ];
        $artiste = new Artiste();
        $artiste->setNom("Affleck")->setPrenom("Ben")->setPhoto("ben.jpg");
        $manager->persist($artiste);

        $film= new Film();
        $film->setTitre("Will Hunting")->setDateDeSortie( new \DateTime("1998-01-01"))->setDescription("Un pauvre se découvre génie des maths")->setAffiche("willHunting.jpg")->setRealisateur( $artiste );
        
        $manager->persist($film);
     
        $manager->flush();
    }
}
