<?php

namespace App\Controller;

use App\Entity\Artiste;
use App\Entity\Film;
use App\Repository\FilmRepository;
use App\Repository\ArtisteRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front")
     */
    public function index(FilmRepository $filmRepository, ArtisteRepository $artisteRepository): Response
    {

        $films= $filmRepository->findAll();
        $artistes = $artisteRepository->findAll();

        return $this->render('front/index.html.twig', [
            'films' => $films,
            "artistes"=>$artistes
        ]
    );
    }

    /**
     * @Route("/fiche-film/{id}", name="film_front", methods={"GET"})
     */
    public function showFilm(Film $film): Response
    {
        return $this->render('front/show-film.html.twig', [
            'film' => $film,
        ]);
    }

    /**
     * @Route("/fiche-artiste/{id}", name="artiste_front", methods={"GET"})
     */
    public function showArtiste(Artiste $artiste): Response
    {
        return $this->render('front/show-artiste.html.twig', [
            'artiste' => $artiste,
        ]);
    }
}
